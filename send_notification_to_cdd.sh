#!/bin/bash

export CDD_APPLICATION_NAME=$BITBUCKET_REPO_SLUG
export CDD_APPLICATION_VERSION_NAME=$BITBUCKET_BRANCH
export CDD_GIT_COMMIT_ID=$BITBUCKET_COMMIT
export CDD_APPLICATION_VERSION_BUILD_NUMBER=$BITBUCKET_BUILD_NUMBER


curl -s --header "Content-Type: application/json" --header "Accept: application/json" -d \
"{ \"applicationName\": \"$CDD_APPLICATION_NAME\", \
    \"applicationSourceName\": \"$CDD_APPLICATION_SOURCE\", \
	\"applicationVersionBuildNumber\": \"$CDD_APPLICATION_VERSION_BUILD_NUMBER\", \
	\"applicationVersionName\": \"$CDD_APPLICATION_VERSION_NAME\", \
	\"commits\": [ { \"commitId\": \"$CDD_GIT_COMMIT_ID\" } ]}" \
	"$CDD_SERVER_URL/cdd/design/$CDD_TENANT_ID/v1/applications/application-versions/application-version-builds" \
	-H "Authorization: Bearer $CDD_API_KEY"
